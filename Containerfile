# Build espmonitor first
FROM docker.io/rust:1.63-alpine AS BUILDER

WORKDIR /root
RUN apk add --no-cache musl-dev perl make
RUN cargo install cargo-generate

# This creates the actual finished container
FROM docker.io/alpine:3.16

ENV NAME=cargo-generate ARCH=x86_64
LABEL   name="$NAME" \
        architecture="$ARCH" \
        run="podman run --rm -i -v $PWD:/project:z -w /project IMAGE ..." \
        summary="Fill in cargo project templates" \
        maintainer="Andreas Hartmann <hartan@7x.de>" \
        url="https://gitlab.com/c8160/embedded-rust/cargo-generate"

# Move espmonitor to a new container
COPY --from=BUILDER /usr/local/cargo/bin/cargo-generate /usr/bin/cargo-generate

# Copy documentation into container
COPY help.md /

VOLUME /project
WORKDIR /project

ENTRYPOINT [ "/usr/bin/cargo-generate", "generate" ]
