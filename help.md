cargo-generate image for running cargo-generate from a container.

Packages the [cargo-generate utility](https://github.com/cargo-generate/cargo-generate).

Volumes:
PWD: Mapped to "$PWD" in the container, used to exchange files with the host.
  Without this, the created projects will not persist on disk and the host can
  not use them. Note that when mounting only the PWD, paths further up in the
  file tree can not be accessed e.g. with the `--path` argument.

Documentation:
For the underlying cargo-generate project, see https://github.com/cargo-generate/cargo-generate
For this container, see https://gitlab.com/c8160/embedded-rust/cargo-generate

Requirements:
None
