# cargo-generate

cargo-generate based on Alpine Linux.

> This image is tested for use with **podman** on **Linux** hosts with **x86_64** arch


## What is cargo-generate?

cargo-generate is a developer tool to help you get up and running quickly with
a new Rust project by leveraging a pre-existing git repository as a template.

See [the project website](https://github.com/cargo-generate/cargo-generate).


## How to use this image

### As a replacement for "regular" cargo-generate

```bash
$ podman run --rm -it -v "$PWD:$PWD:z" -w "$PWD" registry.gitlab.com/c8160/embedded-rust/cargo-generate --help
```

This will run cargo-generate with `--help` as argument.

Note that a native installation of `cargo-generate` integrates into your
existing `cargo` installation as subcommand, i.e. `cargo generate`. This
container offers only the standalone `cargo-generate` executable, and hence
will never integrate into your hosts cargo executable.


## Getting help

If you feel that something isn't working as expected, open an issue in the
Gitlab project for this container and describe what issue you are facing.

